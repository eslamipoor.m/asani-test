﻿using AsaNi.Infrastructure.DataModels;
using AsaNi.Infrastructure.DataModels.Identity;
using AsaNi.Infrastructure.EntityConfiguration;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace AsaNi.Infrastructure
{
    public class AsaNiContext : IdentityDbContext<User, Role, int,
                                IdentityUserClaim<int>, UserRole, IdentityUserLogin<int>,
                                IdentityRoleClaim<int>, IdentityUserToken<int>>
    {

        public AsaNiContext(DbContextOptions<AsaNiContext> options) :
         base(options)
        {
        }
        public DbSet<PropertyEF> Properties { get; set; }
        public DbSet<OwnerEF> Owners { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfiguration<UserRole>(new UserRoleConfiguration());
            builder.ApplyConfiguration<PropertyEF>(new PropertyConfiguration());
            builder.ApplyConfiguration<OwnerEF>(new OwnerConfiguration());

            base.OnModelCreating(builder);
        }
    }
}
