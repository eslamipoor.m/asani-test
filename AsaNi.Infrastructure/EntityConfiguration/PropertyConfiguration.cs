﻿using AsaNi.Infrastructure.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;

namespace AsaNi.Infrastructure.EntityConfiguration
{
    public class PropertyConfiguration : IEntityTypeConfiguration<PropertyEF>
    {
        public void Configure(EntityTypeBuilder<PropertyEF> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.Shomare);
            builder.Property(p => p.Metrajh);
            builder.Property(p => p.Address);
            builder.Property(p => p.IsNorth);
            builder.HasOne(p => p.Owner).WithMany();
            builder.Property(p => p.IsDeleted);
            builder.HasOne(p => p.User).WithMany();
            builder.Property(p => p.ModifiedDate);
            builder.Property(p => p.Action).HasConversion(v => v.GetHashCode(),
                                                          v => (Base.Entity.Action)Enum.Parse(typeof(Base.Entity.Action), v.ToString()));
        }
    }
}
