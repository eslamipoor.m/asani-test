﻿using AsaNi.Infrastructure.DataModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace AsaNi.Infrastructure.EntityConfiguration
{
    public class OwnerConfiguration : IEntityTypeConfiguration<OwnerEF>
    {
        public void Configure(EntityTypeBuilder<OwnerEF> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.Name);
            builder.Property(p => p.LastName);
            builder.Property(p => p.PhoneNumber);
        }
    }
}
