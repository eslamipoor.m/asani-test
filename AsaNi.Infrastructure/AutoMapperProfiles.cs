﻿using AsaNi.Core.Entities;
using AsaNi.Infrastructure.DataModels;
using AutoMapper;

namespace AsaNi.Infrastructure
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Property, PropertyEF>();
            CreateMap<PropertyEF, Property>();

            CreateMap<Owner, OwnerEF>();
            CreateMap<OwnerEF, Owner>();

            CreateMap<DataModels.Identity.User, Core.Entities.Identity.User>();
            CreateMap<Core.Entities.Identity.User, DataModels.Identity.User>();


        }
    }
}
