﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsaNi.Infrastructure.DataModels
{
    public class OwnerEF
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
