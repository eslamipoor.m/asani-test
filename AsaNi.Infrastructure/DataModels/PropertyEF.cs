﻿using AsaNi.Infrastructure.DataModels.Identity;
using System;

namespace AsaNi.Infrastructure.DataModels
{
    public class PropertyEF
    {
        public int Id { get; set; }
        public int Shomare { get; set; }
        public string Name { get; set; }
        public double Metrajh { get; set; }
        public string Address { get; set; }
        public bool IsNorth { get; set; }
        public int OwnerId { get; set; }
        public bool IsDeleted { get; set; }
        public int UserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Base.Entity.Action Action { get; set; }

        public OwnerEF Owner { get; set; }
        public User User { get; set; }
    }
}
