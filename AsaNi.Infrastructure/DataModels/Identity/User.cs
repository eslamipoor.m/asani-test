﻿using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace AsaNi.Infrastructure.DataModels.Identity
{
    public class User : IdentityUser<int>
    {
        public ICollection<UserRole> UserRoles { get; set; }
    }
}
