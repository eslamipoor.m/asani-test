﻿using Microsoft.AspNetCore.Identity;

namespace AsaNi.Infrastructure.DataModels.Identity
{
    public class UserRole : IdentityUserRole<int>
    {
        public User User { get; set; }

        public Role Role { get; set; }
    }
}
