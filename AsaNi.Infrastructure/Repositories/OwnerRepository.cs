﻿using AsaNi.Base.Utility;
using AsaNi.Core.Entities;
using AsaNi.Core.Repositories;
using AsaNi.Infrastructure.DataModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNi.Infrastructure.Repositories
{
    public class OwnerRepository : IOwnerRepository
    {
        private readonly AsaNiContext _asaNiContext;
        private readonly IMapperFacade _mapper;

        public OwnerRepository(AsaNiContext asaNiContext, IMapperFacade mapper)
        {
            _asaNiContext = asaNiContext;
            _mapper = mapper;
            _asaNiContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<List<Owner>> GetAll()
        {
            var owners = await _asaNiContext.Owners.ToListAsync();
         
            return _mapper.Map<List<OwnerEF>, List<Owner>>(owners);
        }

        public async Task Insert(Owner owner)
        {
            var insertedOwner = await _asaNiContext.AddAsync(_mapper.Map<Owner, OwnerEF>(owner));
            _asaNiContext.Entry<OwnerEF>(insertedOwner.Entity).State = EntityState.Added;

            await _asaNiContext.SaveChangesAsync();
        }
    }
}
