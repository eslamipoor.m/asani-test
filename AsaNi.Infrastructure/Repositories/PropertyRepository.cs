﻿using AsaNi.Base.Utility;
using AsaNi.Core.Entities;
using AsaNi.Core.Repositories;
using AsaNi.Infrastructure.DataModels;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsaNi.Infrastructure.Repositories
{
    public class PropertyRepository : IPropertyRepository
    {
        private readonly IMapperFacade _mapper;
        private readonly AsaNiContext _asaNiContext;

        public PropertyRepository(IMapperFacade mapper, AsaNiContext asaNiContext)
        {
            _mapper = mapper;
            _asaNiContext = asaNiContext;
            _asaNiContext.ChangeTracker.QueryTrackingBehavior = QueryTrackingBehavior.NoTracking;
        }

        public async Task<List<Property>> GetAll()
        {
            var propertyList = await _asaNiContext.Properties
                                                 .Include(p => p.Owner)
                                                 .Include(p => p.User)
                                                 .ToListAsync();

            return _mapper.Map<List<PropertyEF>, List<Property>>(propertyList);
        }

        public async Task<Property> Get(int id)
        {
            var property = await _asaNiContext.Properties
                                             .Include(p => p.Owner)
                                             .Include(p => p.User)
                                             .Where(p => p.Id == id)
                                             .FirstOrDefaultAsync();

            return _mapper.Map<PropertyEF, Property>(property);
        }

        public async Task<Property> Insert(Property property)
        {
            var insertedProperty = await _asaNiContext.AddAsync(_mapper.Map<Property, PropertyEF>(property));
            _asaNiContext.Entry<PropertyEF>(insertedProperty.Entity).State = EntityState.Added;

            await _asaNiContext.SaveChangesAsync();

            return _mapper.Map<PropertyEF, Property>(await _asaNiContext.Properties.Include(p => p.Owner)
                                                                                  .Where(p => p.Id == insertedProperty.Entity.Id)
                                                                                  .FirstOrDefaultAsync());
        }

        public async Task Update(Property property)
        {
            var updatedProperty = _mapper.Map<Property, PropertyEF>(property);

            _asaNiContext.Entry<PropertyEF>(updatedProperty).State = EntityState.Modified;
            await _asaNiContext.SaveChangesAsync();
        }

        public async Task Delete(Property property)
        {
            var deletedProperty = _mapper.Map<Property, PropertyEF>(property);

            _asaNiContext.Entry<PropertyEF>(deletedProperty).State = EntityState.Modified;
            await _asaNiContext.SaveChangesAsync();
        }
    }
}
