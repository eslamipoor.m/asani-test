﻿using AsaNi.Base.Utility;
using AsaNi.Core.Repositories;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace AsaNi.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly UserManager<DataModels.Identity.User> _userManager;
        private readonly SignInManager<DataModels.Identity.User> _signInManager;
        private readonly IMapperFacade _mapper;
        private readonly IConfiguration _configuration;

        public UserRepository(UserManager<DataModels.Identity.User> userManager, SignInManager<DataModels.Identity.User> signInManager,
                                IMapperFacade mapper, IConfiguration configuration)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _mapper = mapper;
            _configuration = configuration;
        }

        public async Task<string> GenerateJwtToken(Core.Entities.Identity.User loginUser)
        {
            var user = _mapper.Map<Core.Entities.Identity.User, DataModels.Identity.User>(loginUser);
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()),
                new Claim(ClaimTypes.Name, user.UserName),
            };

            var roles = await _userManager.GetRolesAsync(user);

            foreach (var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role));
            }

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration.GetSection("AppSettings:Token").Value));

            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }

        public async Task<Core.Entities.Identity.User> Login(Core.Entities.Identity.User user)
        {
            var findUser = await _userManager.FindByNameAsync(user.UserName);

            var result = await _signInManager.CheckPasswordSignInAsync(findUser, user.Password, false);
            if (result.Succeeded)
            {
                var appUser = await _userManager.Users.FirstOrDefaultAsync(u => u.NormalizedUserName == user.UserName.ToUpper());
                return _mapper.Map<DataModels.Identity.User, Core.Entities.Identity.User>(appUser);
            }

            return null;
        }

        public async Task<bool> Register(Core.Entities.Identity.User user)
        {
            var result = await _userManager.CreateAsync(_mapper.Map<Core.Entities.Identity.User, DataModels.Identity.User>(user), user.Password);
            return result.Succeeded;
        }
    }
}
