﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace AsaNi.Infrastructure
{
    public class AsaNiContextFactory : IDesignTimeDbContextFactory<AsaNiContext>
    {
        private readonly IConfiguration _configuration;

        public AsaNiContextFactory(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        public AsaNiContext CreateDbContext(string[] args)
        {
            var optionsBuilder = new DbContextOptionsBuilder<AsaNiContext>();
            optionsBuilder.UseSqlServer(_configuration.GetConnectionString("DefaultConnection"));

            return new AsaNiContext(optionsBuilder.Options);
        }
    }
}
