using AsaNi.Core.Entities;
using AsaNi.Core.Repositories;
using AsaNi.Infrastructure;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace AsaNi.API
{
    public class Seed
    {
        private readonly IOwnerRepository _ownerRepository;
        private readonly AsaNiContext _asaNiContext;

        public Seed(IOwnerRepository ownerRepository, AsaNiContext asaNiContext)
        {
            _ownerRepository = ownerRepository;
            _asaNiContext = asaNiContext;
        }
        public void SeedOwners()
        {
            if (!_asaNiContext.Owners.Any())
            {
                var ownerData = System.IO.File.ReadAllText("Seed/Owner.Json");
                var owners = JsonConvert.DeserializeObject<List<Owner>>(ownerData);

                foreach (var owner in owners)
                {
                    _ownerRepository.Insert(owner);
                }
            }
        }
    }
}
