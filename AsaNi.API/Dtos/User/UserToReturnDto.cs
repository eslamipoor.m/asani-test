namespace AsaNi.API.Dtos.User
{
    public class UserToReturnDto
    {
        public int Id { get; set; }
        public string Username { get; set; }
    }
}
