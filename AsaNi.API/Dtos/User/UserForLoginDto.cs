using System.ComponentModel.DataAnnotations;

namespace AsaNi.API.Dtos.User
{
    public class UserForLoginDto
    {
        [Required]
        public string UserName { get; set; }

        [Required, MinLength(5), MaxLength(10)]
        public string Password { get; set; }
    }
}
