using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AsaNi.API.Dtos.User
{
    public class UserForRegistration
    {
        [Required]
        public string UserName { get; set; }

        [Required, MinLength(5), MaxLength(10)]
        public string Password { get; set; }

        public DateTime Created { get; set; }

        public DateTime LastActive { get; set; }

        public UserForRegistration()
        {
            Created = DateTime.Now;
            LastActive = DateTime.Now;
        }
    }
}
