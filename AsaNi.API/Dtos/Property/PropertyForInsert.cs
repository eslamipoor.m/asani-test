namespace AsaNi.API.Dtos.Property
{
    public class PropertyForInsert
    {
        public int Shomare { get; set; }
        public string Name { get; set; }
        public double Metrajh { get; set; }
        public string Address { get; set; }
        public bool IsNorth { get; set; }
        public int OwnerId { get; set; }
    }
}
