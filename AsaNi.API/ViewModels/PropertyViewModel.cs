using System;

namespace AsaNi.API.ViewModels
{
    public class PropertyViewModel
    {
        public int Shomare { get; set; }
        public string Name { get; set; }
        public double Metrajh { get; set; }
        public string Address { get; set; }
        public bool IsNorth { get; set; }
        public string OwnerName { get; set; }
        public string OwnerLastName { get; set; }
        public string UserName { get; set; }
        public string Action { get; set; }
        public DateTime ModifiedDate { get; set; }

    }
}
