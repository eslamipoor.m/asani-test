using AsaNi.API.Dtos.Property;
using AsaNi.API.ViewModels;
using AsaNi.Base.Utility;
using AsaNi.Core.Entities;
using AsaNi.Core.Repositories;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AsaNi.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PropertyController : ControllerBase
    {
        private readonly IPropertyRepository _propertyRepository;
        private readonly IMapperFacade _mapper;

        public PropertyController(IPropertyRepository propertyRepository, IMapperFacade mapper)
        {
            _propertyRepository = propertyRepository;
            _mapper = mapper;
        }

        [HttpGet("getAll")]
        public async Task<List<PropertyViewModel>> GetAll()
        {
            var propertyList = await _propertyRepository.GetAll();
            return _mapper.Map<List<Property>, List<PropertyViewModel>>(propertyList);
        }

        [HttpGet("get/{id}")]
        public async Task<PropertyViewModel> Get(int id)
        {
            var property = await _propertyRepository.Get(id);
            return _mapper.Map<Property, PropertyViewModel>(property);
        }

        [HttpPost("create")]
        public async Task<PropertyViewModel> Create(PropertyForInsert propertyForInsert)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var property = Property.Create(propertyForInsert.Shomare, propertyForInsert.Name, propertyForInsert.Metrajh,
                                            propertyForInsert.Address, propertyForInsert.IsNorth, propertyForInsert.OwnerId, Convert.ToInt16(userId));
            
            var insertedProperty = await _propertyRepository.Insert(property);
            return _mapper.Map<Property, PropertyViewModel>(insertedProperty);
        }

        [HttpPut("update")]
        public async Task<ActionResult<PropertyViewModel>> Update(PropertyForUpdate propertyForUpdate)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var property = _mapper.Map<PropertyForUpdate, Property>(propertyForUpdate);
            property.Update(Convert.ToInt16(userId));

            await _propertyRepository.Update(property);

            return _mapper.Map<Property, PropertyViewModel>(property);
        }

        [HttpDelete("delete/{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            var userId = HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value;
            var property = await _propertyRepository.Get(id);
            property.Delete(Convert.ToInt16(userId));
            await _propertyRepository.Delete(property);

            return Ok();
        }
    }
}
