using AsaNi.API.Dtos.User;
using AsaNi.Base.Utility;
using AsaNi.Core.Entities.Identity;
using AsaNi.Core.Repositories;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace AsaNi.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : ControllerBase
    {
        private readonly IMapperFacade _mapper;
        private readonly IUserRepository _userRepository;

        public AccountController(
           IMapperFacade mapper,
           IUserRepository userRepository)
        {
            _mapper = mapper;
            _userRepository = userRepository;
        }

        [HttpPost("register")]
        public async Task<IActionResult> Register(UserForRegistration userDtoModel)
        {
            var userToCreate = _mapper.Map<UserForRegistration, User>(userDtoModel);
            var result = await _userRepository.Register(userToCreate);
            var userToReturn = _mapper.Map<User, UserToReturnDto>(userToCreate);

            if (result)
            {
                return Ok(userToReturn);
            }

            return BadRequest();
        }

        [HttpPost("login")]
        public async Task<IActionResult> Login(UserForLoginDto userDtoModel)
        {
            var appUser = await _userRepository.Login(_mapper.Map<UserForLoginDto, User>(userDtoModel));
            if (appUser != null)
            {
                var userToReturn = _mapper.Map<User, UserToReturnDto>(appUser);

                return Ok(new
                {
                    token = _userRepository.GenerateJwtToken(appUser),
                    user = userToReturn
                });
            }
            return Unauthorized();
        }

        [HttpGet("logout")]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync(
                CookieAuthenticationDefaults.AuthenticationScheme);

            return Ok();
        }

    }
}
