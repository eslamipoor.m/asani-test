using AsaNi.API.Dtos.Property;
using AsaNi.API.Dtos.User;
using AsaNi.API.ViewModels;
using AsaNi.Core.Entities;
using AsaNi.Core.Entities.Identity;
using AutoMapper;

namespace AsaNi.API
{
    public class AutoMapperProfiles : Profile
    {
        public AutoMapperProfiles()
        {
            CreateMap<Property, PropertyViewModel>()
                .ForMember(dest => dest.OwnerName, opt => opt.MapFrom(src => src.Owner.Name))
                .ForMember(dest => dest.OwnerLastName, opt => opt.MapFrom(src => src.Owner.LastName))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName))
                .ForMember(dest => dest.Action, opt => opt.MapFrom(src => src.Action.ToString()));



            CreateMap<PropertyForUpdate, Property>();

            CreateMap<User, UserToReturnDto>();
            CreateMap<UserForRegistration, User>();
            CreateMap<UserForLoginDto, User>();
        }
    }
}
