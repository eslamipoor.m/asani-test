using AsaNi.Base.Utility;
using AutoMapper;

namespace AsaNi.API
{
    public class AutomapperFacade : IMapperFacade
    {
        private readonly MapperConfiguration mapperConfiguration;
        public AutomapperFacade(params Profile[] profiles)
        {
            mapperConfiguration = new MapperConfiguration(c =>
            {
                foreach (var item in profiles)
                {
                    c.AddProfile(item);
                }
            });
        }
        public TOutput Map<TInput, TOutput>(TInput input)
        {
            return mapperConfiguration.CreateMapper().Map<TOutput>(input);
        }
    }
}
