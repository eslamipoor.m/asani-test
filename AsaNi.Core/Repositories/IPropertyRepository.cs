﻿using AsaNi.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNi.Core.Repositories
{
    public interface IPropertyRepository
    {
        Task<Property> Insert(Property property);
        Task Update(Property property);
        Task<Property> Get(int id);
        Task<List<Property>> GetAll();
        Task Delete(Property property);
    }
}
