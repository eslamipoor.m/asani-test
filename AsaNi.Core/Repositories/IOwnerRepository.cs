﻿using AsaNi.Core.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AsaNi.Core.Repositories
{
    public interface IOwnerRepository
    {
        Task Insert(Owner owner);
        Task<List<Owner>> GetAll();
    }
}
