﻿using AsaNi.Core.Entities.Identity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AsaNi.Core.Repositories
{
    public interface IUserRepository
    {
        Task<bool> Register(User user);
        Task<User> Login(User user);
        Task<string> GenerateJwtToken(User user);
    }
}
