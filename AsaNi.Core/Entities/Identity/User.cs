﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AsaNi.Core.Entities.Identity
{
    public class User
    {
        public int Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public string Password { get; set; }

    }
}
