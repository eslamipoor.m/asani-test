﻿using AsaNi.Base.Entity;

namespace AsaNi.Core.Entities
{
    public class Owner : BaseEntity
    {
        public string Name { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
