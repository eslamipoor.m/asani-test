﻿using AsaNi.Base.Entity;
using AsaNi.Core.Entities.Identity;
using System;

namespace AsaNi.Core.Entities
{
    public class Property : BaseEntity, ILogicalDelete, ILogEntity
    {
        #region Properties

        public int Shomare { get; private set; }
        public string Name { get; private set; }
        public double Metrajh { get; private set; }
        public string Address { get; private set; }
        public bool IsNorth { get; private set; }
        public int OwnerId { get; private set; }
        public bool IsDeleted { get; set; }
        public int UserId { get; set; }
        public DateTime ModifiedDate { get; set; }
        public Base.Entity.Action Action { get; set; }

        #endregion

        #region Navigation Properties
        public Owner Owner { get; set; }
        public User User { get; set; }

        #endregion

        #region Constructor
        protected Property()
        {

        }
        public Property(int id, int shomare, string name, double metrajh, string address, bool isNorth, int ownerId, bool isDeleted, int userId, DateTime modifiedDate, Base.Entity.Action action)
        {
            Id = id;
            Shomare = shomare;
            Name = name;
            Metrajh = metrajh;
            Address = address;
            IsNorth = isNorth;
            OwnerId = ownerId;
            IsDeleted = isDeleted;
            UserId = userId;
            ModifiedDate = modifiedDate;
            Action = action;
        }
        #endregion

        #region Methods

        public static Property Create(int shomare, string name, double metrajh, string address, bool isNorth, int ownerId, int userId)
        {
            return new Property(0, shomare, name, metrajh, address, isNorth, ownerId, false, userId, DateTime.Now, Base.Entity.Action.Create);
        }
        public void Update(int userId)
        {
            UserId = userId;
            ModifiedDate = DateTime.Now;
            Action = Base.Entity.Action.Modified;
        }

        public void Delete(int userId)
        {
            IsDeleted = true;
            UserId = userId;
            ModifiedDate = DateTime.Now;
            Action = Base.Entity.Action.Delete;
        }
        #endregion
    }
}
