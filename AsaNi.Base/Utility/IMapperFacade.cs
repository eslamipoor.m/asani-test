﻿namespace AsaNi.Base.Utility
{
    public interface IMapperFacade
    {
        TOutput Map<TInput, TOutput>(TInput input);
    }
}
