﻿namespace AsaNi.Base.Entity
{
    public enum Action
    {
        Create = 1,
        Modified = 2,
        Delete = 3
    }
}
