﻿namespace AsaNi.Base.Entity
{
    public interface ILogicalDelete
    {
        bool IsDeleted { get; set; }
    }
}
