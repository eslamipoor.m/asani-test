﻿using System;

namespace AsaNi.Base.Entity
{
    public interface ILogEntity
    {
        int UserId { get; set; }
        DateTime ModifiedDate { get; set; }
        Action Action { get; set; }
    }
}
